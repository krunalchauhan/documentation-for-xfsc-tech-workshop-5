# Deploying OCM-Engine in Your Cloud Provider

This guide will walk you through the process of deploying the OCM-Engine in your preferred cloud provider environment. Ensure you have the necessary prerequisites set up before proceeding with the deployment.

## Prerequisites

Before deploying the OCM-Engine, make sure you have the following components and tools available in your environment:

-   Container Engine
-   Kubernetes Cluster
-   NGINX Ingress Controller
-   PostgreSQL Database
-   Helm

## Getting Started

### Clone the OCM-Engine Repository

```sh
git clone https://github.com/smartSenseSolutions/gaia-x-ocm-engine.git
```

## Deploy Following components

1. **Attestation Manager**

    Deploy the `attestation-manager` using the Helm charts provided [here](https://github.com/smartSenseSolutions/gaia-x-ocm-engine/tree/master/apps/attestation-manager/deployment/helm)

    Utilize the `values-override.yaml` for customizing your deployment

2. **Connection Manager**

    Deploy the `connection-manager` using the Helm charts provided [here](https://github.com/smartSenseSolutions/gaia-x-ocm-engine/tree/master/apps/connection-manager/deployment/helm)

    Utilize the `values-override.yaml` for customizing your deployment

3. **Principal Manager**

    Deploy the `principal-manager` based on the provided configurations found [here](https://github.com/smartSenseSolutions/gaia-x-ocm-engine/tree/master/additional_components_deployment/principal-manager)

4. **Proof Manager**

    Deploy the `proof-manager` using the Helm charts provided [here](https://github.com/smartSenseSolutions/gaia-x-ocm-engine/tree/master/apps/proof-manager/deployment/helm)

    Utilize the `values-override.yaml` for customizing your deployment

5. **SSI Abstraction**

    Deploy the `ssi-abstraction` using the Helm charts provided [here](https://github.com/smartSenseSolutions/gaia-x-ocm-engine/tree/master/apps/ssi-abstraction/deployment/helm)

    Utilize the `values-override.yaml` for customizing your deployment

6. **Additional Components**

    Deploy additional components such as `nats` and `aries-mediator` using the configurations provided below

    - [Nats](https://github.com/smartSenseSolutions/gaia-x-ocm-engine/tree/master/additional_components_deployment/nats)

    - [Aries mediator](https://github.com/smartSenseSolutions/gaia-x-ocm-engine/tree/master/additional_components_deployment/aries-mediator)
